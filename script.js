/**
 * VARIABLES
 */

// EXERCICE 1
var a = 10;
var b = 15;
var r = 0;
r = a + b - r;
console.log(r); // Résultat : 25

// EXERCICE 2
var a = 10;
var b = 15;
var r = 0;
r = a * a * b / b + (r - a);
console.log(r); // Résultat : 90

// EXERCICE 3
var a = 10;
var b = 15;
var r = 0;
r = b > a < r;
console.log(r); // Résultat : false